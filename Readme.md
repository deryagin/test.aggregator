# ОПИСАНИЕ ЗАДАНИЯ

- Предположим, что есть система, которая собирает и агрегирует много разнородных данных. Например, это dashboard, на который выводится много показателей. Этой системе нужен некий механизм, который данные из множества сторонних источников выдает в едином формате, подходящем для графиков. Примеры источников данных: оплаченные счета, погода, курсы валют, данные о заказах из CRM и так далее. На вход механизму подается период времени, за который нужна информация. В ответ система получает массив данных с группировкой по датам. Например, {10.02.2015: 10, 11.02.2015: 21, ...}.

- Спроектируй такой механизм, который позволил бы постоянно добавлять новые источники данных, не затрагивая при этом единожды написанный код системы. Например, это может быть абстрактный класс с описанием множества методов. А для каждого источника данных делается свой наследуемый класс. При этом постарайся учесть, как можно больше факторов. Например, у разных API могут быть совершенно разные способы аутентификации. Также у них разные форматы выдачи данных. Подсказка: пример подобной системы из реальной жизни — Munin.

- Результат задания необходимо оформить в виде ссылки на публичный git репозиторий (github или любой другой).



# АНАЛИЗ ЗАДАЧИ

- В текущей постановке задачи можно выделить несколько наиболее вероятных изменений:
	1. Период времени, вероятно будет не единственным параметром на входе.
	2. Массив данных с группировкой по датам, вероятно будет не единственным выходным форматом.



# ПОДХОД К РЕШЕНИЮ

- Подход на основе общего абстрактного класса со множеством наследников представляется не совсем удобным. Т.к. предполагает наличие одинакового главного цикла обработки. Очевидно, что какие-то источники будут иметь аутентификацию, а какие-то нет. Различные транспорты возможно потребуют разного количества шагов. Итп.

- Все это обязательно приведет к усложнению интерфейса абстрактного класса, нарушению ISP, появлению всяких пустых методов, оверайду методов итп. Действительно важным представляются только 3 вещи:
	1. `src/Abstraction/IDataSource.php` - интерфейс для запроса к источнику данных.
	2. `src/Abstraction/IDiagramData.php` - интерфейс для ответа, от источника данных.
	3. `lib/native/Error/ModuleException.php` - интерфейс для информирования об ошибке.

- Очевидно, что интерфейсы запроса и ответа (`IDataSource` и `IDiagramData`) должны быть заданы в самом общем виде. Т.к. это 2 наиболее вероятных изменения в будущем. При этом конкретные детали запросов будут передаваться как массив параметров, получаемых из конфига.

- При старте, основной скрипт будет читать доступные конфиги (см. ниже). Каждый из которых хранит всю необходимую информацию для использования модуля, выполняющего получение внешних данных, а так же необходимые параметры запроса.

- Т.о. каждый модуль не будет зависеть ни от каких абстрактных классов. Как следствие сможет сам выбирать алгоритм работы, и выполнять только  нужные шаги. Единственное, что от модуля требуется - это поддержка 3 интерфейсов: для запроса/ответа/ошибки.

- Также, при отказе от использования абстрактного класса отпадает необходимость "учитывать как можно больше факторов" на ранних/любых стадиях разработки новых модулей доступа к источникам данных. Обобщение станет происходить более естественным способом. Вначале, в какой-то модуль добавляется новый метод аутентификации, формат сообщения, логгер, транспорт итп. А при необходимости дальнейшего использования этого функционала, он будеть обобщаться и выноситься в `lib/native/`. Сами модули находятся в `src/DataSource/Module`.



# КОНФИГУРИРОВАНИЕ

- Добавление новых модулей доступа к данным не должно затрагивать существующий код. Это одно из ключевых требований в задаче. Поэтому конфигурирование было вынесено в отдельную директорию `cfg/` в корне проекта. В ней еще 2 субдиректории:
	1. `cfg/Module` - содержит конфиги, для запуска различных модулей.
	2. `cfg/Enabled` - содержит симлинки на конфиги из `cfg/Module`.

- Такая структура позволяет иметь несколько конфигов с разными параметрами для одного и того же модуля. Например модуль `src/DataSource/Module/Accounting` позволяет получать информацию о счетах. А конкретные конфиги `cfg/Module/Accounting.Paid.ini` и `cfg/Module/Accounting.Unpaid.ini` содержат параметры, позволяющие получить оплаченные и неоплаченные счета соответственно.

- Симлинки в папке `cfg/Enabled` позволяют включать/выключать конфиги из `cfg/Module` простым созданием/удалением симлинка. Тем самым включая или выключая получение информации из соответствующего источника данных.

- Ниже приведен пример конфига из `cfg/Module/Accounting.Paid.ini`. Он содержит информацию о самом модуле в секции `[module]`. В секции `[paramSet]` содержится набор параметров, которые должны передаваться при запросе к источнику данных в метод `IDataSource::process($paramSet)`.
```ini
	[module]
	name = "Accounting"
	description = "20 последних оплаченных счетов."
	handler = "src/DataSource/Module/Accounting/DataSource.php"

	[paramSet]
	filter = "paid"
	limit = 20
	orderBy = "date desc"
```

- Т.о., для добавления нового модуля необходимо сделать следующие шаги:
	1. Создать модуль в `src/DataSource/Module`.
	2. Создать нужное количество конфигов в `cfg/Module`.
	3. Включить нужные конфиги, установив на них симлинк в `cfg/Enabled`.

- В исходной постановке задачи предполагалась передача динамических параметров запроса (интервал времени). Это можно сделать динамически формируя массив `$paramSet` для метода `IDataSource::process($paramSet)`, вместо того, чтобы брать его из конфига.

- Предполагается, что все модули в `src/DataSource/Module` имеют одинаковую файловую структуру. Если это не так, то в конфиг можно включить информацию об автозагрузке для автолоадера.



# СТРУКТУРА ПРОЕКТА
```sh
	Aggregator
	├── Readme.md - файл, который вы сейчас читаете
	├── cfg - конфигурирование
	│   ├── Enabled - симлинки на конфиги из cfg/Module
	│   │   ├── Accounting.Paid.ini -> ../Module/Accounting.Paid.ini
	│   │   ├── CRMOrder.ini -> ../Module/CRMOrder.ini
	│   │   ├── Currency.EUR.ini -> ../Module/Currency.EUR.ini
	│   │   └── Weather.Moscow.ini -> ../Module/Weather.Moscow.ini
	│   └── Module - конфиги для модулей доступа к данным из src/DataSource/Module
	│       ├── Accounting.Paid.ini
	│       ├── Accounting.Unpaid.ini
	│       ├── CRMOrder.ini
	│       ├── Currency.EUR.ini
	│       ├── Currency.USD.ini
	│       └── Weather.Moscow.ini
	├── lib
	│   ├── native - "родные" библиотеки, общий функционал для модулей
	│   │   ├── Auth
	│   │   │   ├── BasicAuth
	│   │   │   ├── CertAuth
	│   │   │   └── TokenAuth
	│   │   ├── Error
	│   │   │   └── ModuleException.php
	│   │   ├── Format
	│   │   │   ├── Html
	│   │   │   ├── Json
	│   │   │   ├── Soap
	│   │   │   └── Xml
	│   │   ├── Logger
	│   │   │   ├── FileLogger
	│   │   │   ├── MongoLogger
	│   │   │   └── MysqlLogger
	│   │   └── Transport
	│   │       ├── Ftp
	│   │       ├── Http
	│   │       ├── Mysql
	│   │       ├── Ssh
	│   │       └── WebDav
	│   ├── vendor - 3d-party библиотеки
	│   │   └── phpunit
	│   └── composer.json
	├── pub
	│   └── index.php
	└── src
	    ├── Abstraction - основные интерфейсы/абстракции
	    │   ├── IDiagramData.php
	    │   └── IDataSource.php
	    ├── Dashboard
	    └── DataSource
	        ├── Module - модули для доступа к источникам данных
	        │   ├── Accounting
	        │   │   └── DataSource.php - точка входа в модуль, реализует IDataSource
	        │   ├── CRMOrder
	        │   │   └── DataSource.php - точка входа в модуль, реализует IDataSource
	        │   ├── Currency
	        │   │   └── DataSource.php - точка входа в модуль, реализует IDataSource
	        │   └── Weather
	        │       └── DataSource.php - точка входа в модуль, реализует IDataSource
	        └── Validator
```


# НЕЗАТРОНУТЫЕ ВОПРОСЫ

- Вопрос версионирования. Если проект для внутреннего использования, то версионирование можно добавить по мере необходимости. Если проект для внешнего использования (напр. дашборды будут делаться сторонними организациями), то версионирование лучше добавлять сразу.

- Описание интерфейса `IDiagramData`. Наверное будет содержать описание способа отображения и формата самих данных. Такие вещи нужно проектировать на основе конкретных примеров. Иначе получается конь в вакууме.

- Вопрос валидации. Скорей всего описать метаинформацию для графиков и сами данные, используя только программные интерфейсы будет сложно. Поэтому, придется делать валидаторы, проверяющие правильность структуры этих данных. Т.е. правильность данных, возвращаемых методами `IDiagramData::getData()` и `IDiagramData::getInfo()`. Видимо, понадобиться 3 типа валидиторов:
    1. Валидатор описания диаграммы.
    2. Валидатор данных для графика. Нужно для упрощения отладки и автотестов.
    3. Валидатор внешних данных. Важно узнавать об изменении внешнего формата данных как можно скорее.

- Вопрос кэширования. Очевидно, что информация в разных источниках данных будет меняться с разной скоростью. Например, нет смысла запрашивать курсы валют чаще одного раза в час (или день). Поэтому, есть смысл реализовать механизм кэширования. Помещать кэширование внутрь кода доступа к источнику данных не совсем целесообразно. Лучше, если источники данных вообще ничего не будут знать о том, что они кэшируются. Параметры кэширования можно поместить например в секцию `[caching]`. Тогда `Dashboard` будет пытаться вначале получить данные из кэша, если их там нет, то `Dashboard` будет дергать `IDataSource`.

- Вопрос логирования. Аналогично, механизм может быть либо общим, либо индивидуальным. В последнем случае настройки, очевидно, пойдут в конфиг.
