<?php namespace Aggregator\Lib\Error;

/**
 * Базовое исключение для всех модулей.
 */
class ModuleException extends \Exception
{
	/** @var string - имя источника данных */
	protected $moduleName = '';

	public function __construct($moduleName, $message = "", $code = 0, \Exception $previous = null)
	{
		parent::__construct($message, $code, $previous);
		$this->moduleName = $moduleName;
	}

	public function getModuleName()
	{
	    return $this->moduleName;
	}
}