<?php namespace Aggregator\Abstraction;

/**
 * Интерфейс для получения описания диаграммы и самих данных.
 */
interface IDiagramData
{
	/**
	 * Возвращает массив данных для построения графика.
	 *
	 * @return array
	 */
	public function getData();

	/**
	 * Возвращает массив данных, описывающих график (оси, масштаб, легенда итп.).
	 *
	 * @return array
	 */
	public function getInfo();
}