<?php namespace Aggregator\Abstraction;

use \Aggregator\Lib\Error\ModuleException;

/**
 * Интерфейс источника данных.
 */
interface IDataSource
{
    /**
     * Запускает обработку запроса к внешнему источнику данных.
     *
     * @param array $paramSet - массив параметров из конфига.
     * @return IDiagramData - успешная обработка.
     * @throws ModuleException - неуспешная обработка.
     */
    public function process(array $paramSet);
}